use image::{GenericImage, ImageBuffer, Rgb, RgbImage};

use crate::cats::NAND_CAT;
use crate::drawing_util::{copy_transparent, fill, fill_mask, fill_mirrored, ScalingImage};
use crate::schema::{Component, Schema};

const SCALE_X: u32 = 11;
const SCALE_Y: u32 = 9;
const PADDING_X: u32 = 10;
const PADDING_Y: u32 = 10;

const COLOR_BG: Rgb<u8> = Rgb([0x0c, 0x3d, 0x66]);
const COLOR_UNKNOWN: Rgb<u8> = Rgb([0x34, 0x11, 0x4d]);
const COLORS_UPPER: [Rgb<u8>; 3] = [
    Rgb([0xfe, 0x18, 0x0f]),
    Rgb([0xfe, 0x9c, 0x0e]),
    Rgb([0xff, 0xff, 0x09]),
];
const COLORS_LOWER: [Rgb<u8>; 3] = [
    Rgb([0x3a, 0xff, 0x0a]),
    Rgb([0x15, 0xa4, 0xfe]),
    Rgb([0x6b, 0x3c, 0xff]),
];

#[inline]
fn translate_coord(x: u32, y: u32) -> (u32, u32) {
    (x * SCALE_X + PADDING_X / 2, y * SCALE_Y + PADDING_Y / 2)
}

pub fn dimensions_for_schema<S: Schema<I>, I: Iterator<Item = (u32, u32, Component)>>(s: &S) -> (u32, u32) {
    let (w, h) = s.dimensions();
    (w * SCALE_X + PADDING_X, h * SCALE_Y + PADDING_Y)
}

pub fn render<S: Schema<I>, I: Iterator<Item = (u32, u32, Component)>>(schema: &S, scale: u32) -> RgbImage {
    let (sw, sh) = dimensions_for_schema(schema);
    let mut out: RgbImage = ImageBuffer::new(sw * scale, sh * scale);
    let mut scaled = ScalingImage { scale, img: &mut out };
    render_into_buffer(schema, &mut scaled);
    out
}

pub fn render_into_buffer<Im: GenericImage<Pixel = Rgb<u8>>, S: Schema<I>, I: Iterator<Item = (u32, u32, Component)>>(schema: &S, img: &mut Im) {
    {
        // check that img is large enough
        let (sw, sh) = dimensions_for_schema(schema);
        let (ow, oh) = img.dimensions();
        if ow < sw || oh < sh {
            panic!("given output buffer was too small. expected: {:?}, got: {:?}", (sw, sh), (ow, oh))
        }

        // fill background color for padding
        fill(img, 0, 0, sw, sh, COLOR_BG);
    }

    for (x, y, c) in schema.iter() {
        let (ix, iy) = translate_coord(x, y);
        match c {
            Component::None => {}
            Component::Unknown => fill(img, ix, iy, SCALE_X, SCALE_Y, COLOR_UNKNOWN),
            Component::UpperRainbow | Component::LowerRainbow => fill_rainbow(schema, img, x, y, ix, iy, c),
            Component::NandGate => fill_cat(schema, img, x, y, ix, iy, c),
        }
    }
}

fn fill_cat<Im: GenericImage<Pixel = Rgb<u8>>, S: Schema<I>, I: Iterator<Item = (u32, u32, Component)>>
    (schema: &S, img: &mut Im, x: u32, y: u32, ix: u32, iy: u32, comp: Component) {
    let w = schema.get_at_offset(x, y, -1, 0);
    let n = schema.get_at_offset(x, y, 0, -1);
    if w != Some(comp) && n != Some(comp) {
        let sw = get_rainbow(schema, x, y, -1, 1);
        let e = get_rainbow(schema, x, y, 4, 0);
        let se = get_rainbow(schema, x, y, 4, 1);
        let w = get_rainbow(schema, x, y, -1, 0);
        if let Some(w) = w {
            fill_rainbow(schema, img, x, y, ix, iy, w);
        }
        if let Some(sw) = sw {
            fill_rainbow(schema, img, x, y + 1, ix, iy + SCALE_Y, sw);
        }
        if let Some(e) = e {
            fill_rainbow(schema, img, x + 3, y, ix + 3 * SCALE_X, iy, e);
        }
        if let Some(se) = se {
            fill_rainbow(schema, img, x + 3, y + 1, ix + 3 * SCALE_X, iy + SCALE_Y, se);
        }
        copy_transparent(img, &*NAND_CAT, ix, iy - 2, COLOR_BG).unwrap();
    }
}

fn fill_rainbow<Im: GenericImage<Pixel = Rgb<u8>>, S: Schema<I>, I: Iterator<Item = (u32, u32, Component)>>
    (schema: &S, img: &mut Im, x: u32, y: u32, ix: u32, iy: u32, comp: Component) {
    let colors = if comp == Component::UpperRainbow { COLORS_UPPER } else { COLORS_LOWER };

    let n = schema.get_at_offset(x, y, 0, -1);
    let s = schema.get_at_offset(x, y, 0, 1);
    let w = schema.get_at_offset(x, y, -1, 0);
    let e = schema.get_at_offset(x, y, 1, 0);

    let mut xo = (y + 1) % 2;
    let yo = x % 2;
    if y >= (schema.dimensions().1) / 2 {
        // TODO symmetrical
        xo = y % 2;
    }
    // let xo = 0;
    // let yo = 0;

    #[inline]
    fn test_straight(comp: Component, n: Option<Component>, s: Option<Component>, e: Option<Component>, w: Option<Component>) -> bool {
        (n == Some(comp) && s == Some(comp)) || ((n == Some(comp) || s == Some(comp)) && e != Some(comp) && w != Some(comp))
    }

    #[inline]
    fn test_corner(comp: Component, w: Option<Component>, n: Option<Component>, e: Option<Component>, s: Option<Component>) -> bool {
        w == Some(comp) && n == Some(comp) && e != Some(comp) && s != Some(comp)
    }

    if test_straight(comp, n, s, e, w) {
        // │
        fill(img, ix + 1 + xo, iy, 3, SCALE_Y, colors[mir3(comp == Component::UpperRainbow, 0)]);
        fill(img, ix + 4 + xo, iy, 3, SCALE_Y, colors[mir3(comp == Component::UpperRainbow, 1)]);
        fill(img, ix + 7 + xo, iy, 3, SCALE_Y, colors[mir3(comp == Component::UpperRainbow, 2)]);
    } else if test_straight(comp, e, w, n, s) {
        // ─
        fill_mask(img, ix, iy + 0 + yo, SCALE_X, 3, colors[0], COLOR_BG);
        fill_mask(img, ix, iy + 3 + yo, SCALE_X, 3, colors[1], COLOR_BG);
        fill_mask(img, ix, iy + 6 + yo, SCALE_X, 3, colors[2], COLOR_BG);
    } else if test_corner(comp, w, n, e, s) {
        // ┘
        fill_rainbow_corner(img, &colors, ix, iy, xo, yo, false, false);
    } else if test_corner(comp, w, s, e, n) {
        // ┐
        fill_rainbow_corner(img, &colors, ix, iy, xo, yo, false, true);
    } else if test_corner(comp, e, n, w, s) {
        // └
        fill_rainbow_corner(img, &colors, ix, iy, xo, yo, true, false);
    } else if test_corner(comp, e, s, w, n) {
        // ┌
        fill_rainbow_corner(img, &colors, ix, iy, xo, yo, true, true);
    } else {
        fill(img, ix, iy, SCALE_X, SCALE_Y, COLOR_UNKNOWN);
    }
}

fn fill_rainbow_corner<Im: GenericImage<Pixel = Rgb<u8>>>(img: &mut Im, colors: &[Rgb<u8>; 3], ix: u32, iy: u32, xo: u32, yo: u32, mirror_x: bool, mirror_y: bool) {
    // ━┙
    fill_mirrored(img, ix + xo, iy + yo, 11, 9, 0, 0, 1, 3, colors[mir3(mirror_y, 0)], mirror_x, mirror_y);
    fill_mirrored(img, ix + xo, iy + yo, 11, 9, 0, 3, 4, 3, colors[mir3(mirror_y, 1)], mirror_x, mirror_y);
    fill_mirrored(img, ix + xo, iy + yo, 11, 9, 0, 6, 7, 3, colors[mir3(mirror_y, 2)], mirror_x, mirror_y);

    // ─┚
    fill_mirrored(img, ix + xo + 1, iy + yo, 9, 9, 0, 0, 3, 3, colors[mir3(mirror_y, 0)], mirror_x, mirror_y);
    fill_mirrored(img, ix + xo + 1, iy + yo, 9, 9, 3, 0, 3, 6, colors[mir3(mirror_y, 1)], mirror_x, mirror_y);
    fill_mirrored(img, ix + xo + 1, iy + yo, 9, 9, 6, 0, 3, 9, colors[mir3(mirror_y, 2)], mirror_x, mirror_y);
}

fn get_rainbow<S: Schema<I>, I: Iterator<Item = (u32, u32, Component)>>(schema: &S, x: u32, y: u32, xo: i8, yo: i8) -> Option<Component> {
    if let Some(comp) = schema.get_at_offset(x, y, xo, yo) {
        return match comp {
            Component::UpperRainbow | Component::LowerRainbow => Some(comp),
            _ => None,
        };
    }
    None
}

#[inline]
fn mir3(b: bool, i: usize) -> usize {
    if !b {
        i
    } else {
        // could also be written as 2-i but the smaller
        // domain [0; 2] may lead to better optimized code
        match i {
            0 => 2,
            1 => 1,
            2 => 0,
            _ => unreachable!(),
        }
    }
}
