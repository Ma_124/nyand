use image::{ImageFormat, RgbImage};

macro_rules! load_cat {
    ($name: expr) => {
        image::load_from_memory_with_format(include_bytes!(concat!("../cats/", $name, ".png")), ImageFormat::Png).unwrap().to_rgb8()
    };
}

lazy_static! {
    pub(crate) static ref NAND_CAT: RgbImage = load_cat!("nand_cat");
}
