#[macro_use]
extern crate lazy_static;

use image::ImageResult;

use crate::render::render;
use crate::schema::ImageSchema;

pub mod drawing_util;
pub mod render;
pub mod schema;

mod cats;

fn compile(s: &str) -> ImageResult<()> {
    let schema = ImageSchema {
        img: &image::open("examples/".to_string() + s + ".png")?.to_rgb8()
    };

    let out = render(&schema, 1);
    out.save("result/".to_string() + s + ".png")?;
    Ok(())
}

fn main() -> ImageResult<()> {
    compile("XOR-NAND")?;
    compile("OR-NAND")?;
    compile("corners")?;
    Ok(())
}
