use image::error::{ParameterError, ParameterErrorKind};
use image::{GenericImage, GenericImageView, ImageError, ImageResult, Pixel};

#[derive(Debug)]
pub struct ScalingImage<'a, I: GenericImage> {
    pub scale: u32,
    pub img: &'a mut I,
}

impl<'a, I: GenericImage> GenericImageView for ScalingImage<'a, I> {
    type Pixel = <I as GenericImageView>::Pixel;
    type InnerImageView = Self;

    fn dimensions(&self) -> (u32, u32) {
        let (w, h) = self.img.dimensions();
        (w / self.scale, h / self.scale)
    }

    fn bounds(&self) -> (u32, u32, u32, u32) {
        let (x, y, w, h) = self.img.bounds();
        (x * self.scale, y * self.scale, w * self.scale, h * self.scale)
    }

    fn get_pixel(&self, x: u32, y: u32) -> Self::Pixel {
        self.img.get_pixel(x * self.scale, y * self.scale)
    }

    fn inner(&self) -> &Self::InnerImageView {
        self
    }
}

impl<'a, I: GenericImage> GenericImage for ScalingImage<'a, I> {
    type InnerImage = Self;

    fn get_pixel_mut(&mut self, x: u32, y: u32) -> &mut Self::Pixel {
        self.img.get_pixel_mut(x * self.scale, y * self.scale)
    }

    fn put_pixel(&mut self, x: u32, y: u32, pixel: Self::Pixel) {
        fill(self.img, x * self.scale, y * self.scale, self.scale, self.scale, pixel)
    }

    fn blend_pixel(&mut self, x: u32, y: u32, pixel: Self::Pixel) {
        fill(self.img, x * self.scale, y * self.scale, self.scale, self.scale, pixel)
    }

    fn inner_mut(&mut self) -> &mut Self::InnerImage {
        self
    }
}

//noinspection RsBorrowChecker
pub fn fill<I: GenericImage>(img: &mut I, x: u32, y: u32, w: u32, h: u32, p: I::Pixel) {
    for xo in 0..w {
        for yo in 0..h {
            img.put_pixel(x + xo, y + yo, p)
        }
    }
}

#[inline]
pub fn fill_mirrored<I: GenericImage>(img: &mut I, ix: u32, iy: u32, rw: u32, rh: u32, fx: u32, fy: u32, fw: u32, fh: u32, p: I::Pixel, mx: bool, my: bool) {
    let nx = ix + if mx { (rw - fx) - fw } else { fx };
    let ny = iy + if my { (rh - fy) - fh } else { fy };
    fill(img, nx, ny, fw, fh, p)
}

//noinspection RsBorrowChecker
pub fn fill_mask<I: GenericImage<Pixel = P>, P: Pixel + Eq>(img: &mut I, x: u32, y: u32, w: u32, h: u32, p: I::Pixel, mask: I::Pixel) {
    for xo in 0..w {
        for yo in 0..h {
            if img.get_pixel(x + xo, y + yo) == mask {
                img.put_pixel(x + xo, y + yo, p)
            }
        }
    }
}

pub fn copy_transparent<I, O, P: Pixel + Eq>(to: &mut I, from: &O, x: u32, y: u32, except: P) -> ImageResult<()>
where
    I: GenericImage<Pixel = P>,
    O: GenericImageView<Pixel = P>,
{
    // Do bounds checking here so we can use the non-bounds-checking
    // functions to copy pixels.
    if to.width() < from.width() + x || to.height() < from.height() + y {
        return Err(ImageError::Parameter(ParameterError::from_kind(
            ParameterErrorKind::DimensionMismatch,
        )));
    }

    for k in 0..from.height() {
        for i in 0..from.width() {
            let p = from.get_pixel(i, k);
            if p != except {
                to.put_pixel(i + x, k + y, p);
            }
        }
    }
    Ok(())
}
