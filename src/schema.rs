use image::{Rgb, RgbImage};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[non_exhaustive]
pub enum Component {
    None,
    Unknown,
    UpperRainbow,
    LowerRainbow,
    NandGate,
}

impl Component {
    fn from_rgb(pixel: &Rgb<u8>) -> Component {
        match pixel {
            Rgb([255, 0, 0]) => Component::UpperRainbow,
            Rgb([0, 255, 0]) => Component::NandGate,
            Rgb([0, 0, 255]) => Component::LowerRainbow,
            Rgb([255, 255, 255]) => Component::None,
            Rgb(_) => Component::Unknown,
        }
    }
}

impl Default for Component {
    fn default() -> Self {
        Self::None
    }
}

pub trait Schema<I: Iterator<Item = (u32, u32, Component)>> {
    fn get_at(&self, x: u32, y: u32) -> Component;

    fn get_at_offset(&self, x: u32, y: u32, xo: i8, yo: i8) -> Option<Component> {
        let (wx, wy) = self.dimensions();
        let mut nx = x;
        let mut ny = y;
        if xo < 0 {
            nx = nx.wrapping_sub((-xo) as u32)
        } else {
            nx += xo as u32
        }
        if yo < 0 {
            ny = ny.wrapping_sub((-yo) as u32)
        } else {
            ny += yo as u32
        }

        if nx >= wx || ny >= wy {
            return None;
        }

        Some(self.get_at(nx, ny))
    }

    fn iter(&self) -> I;
    fn dimensions(&self) -> (u32, u32);
}

#[derive(Debug)]
pub struct ImageSchema<'a> {
    pub img: &'a RgbImage,
}

impl<'a> Schema<ImageSchemaIter<'a>> for ImageSchema<'a> {
    fn get_at(&self, x: u32, y: u32) -> Component {
        Component::from_rgb(self.img.get_pixel(x, y))
    }

    fn iter(&self) -> ImageSchemaIter<'a> {
        ImageSchemaIter { img: self.img, x: 0, y: 0 }
    }

    fn dimensions(&self) -> (u32, u32) {
        self.img.dimensions()
    }
}

#[derive(Debug)]
pub struct ImageSchemaIter<'a> {
    img: &'a RgbImage,
    x: u32,
    y: u32,
}

impl<'a> Iterator for ImageSchemaIter<'a> {
    type Item = (u32, u32, Component);

    fn next(&mut self) -> Option<Self::Item> {
        if self.x >= self.img.width() {
            self.x = 0;
            self.y += 1;
        }

        if self.y >= self.img.height() {
            None
        } else {
            let pixel = self.img.get_pixel(self.x, self.y);
            let p = (self.x, self.y, Component::from_rgb(pixel));

            self.x += 1;

            Some(p)
        }
    }
}
