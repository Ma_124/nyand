# NYAND Cat!
[inspired by Matt Parker][NYAND YT]

*NYAND Cat!* renders schematics of logic diagrams with nyan-like cats.

Schematic (scaled):

![Schmatic][example-schematic-scaled]

Render:

![Render][example-render]

ANSI Symbol for a NAND Gate:

![ANSI Distinctive Shape Symbol for NAND Gate](https://gitlab.com/Ma_124/pages-static-shared/-/raw/master/nand_ansi.png)

NYAND Cat:

![NYAND Cat](https://gitlab.com/Ma_124/pages-static-shared/-/raw/master/nand_cat.png)

The NAND function is functionally complete which means you can express any logical expression only using NAND Gates.
Nonetheless *NYAND Cat!* comes with cats for NOT, OR, NOR, AND, NAND, XOR, and XNOR gates to allow less verbose diagrams.

## Usage
<!-- TODO -->

*NYAND Cat!* is written in [Rust](https://www.rust-lang.org).

Steps to build from source:

```sh
git clone https://gitlab.com/Ma_124/nyand.git && cd nyand
cargo build --release
./target/release/nyand --help
```

## Schematic
| Color Code | Function            |
|------------|---------------------|
| `#ff0000`  | Upper Rainbow (ROY) |
| `#0000ff`  | Lower Rainbow (GIV) |
| `#00ff00`  | NAND Cat (4x2)      |

- [Example][example-schematic]
- [Defintions in code: `Component::from_rgb`](src/schema.rs)

## Credits
- Nyan Cat was originally drawn by Christopher Torres as "Pop-Tart Cat".
- The name comes from [Saraj00n][NYAN YT].
- The idea and drawing of NYAN**D** Cat comes from [Matt Parker][NYAND YT].

## License
Copyright (c) 2021 Ma_124 <ma_124+oss@pm.me>

Licensed under the [Apache License, Version 2.0](./LICENSE-APACHE) or the [MIT license](./LICENSE-MIT), at your option.
Code in the project may not be copied, modified, or distributed except according to those terms.

[example-schematic-scaled]: https://gitlab.com/Ma_124/pages-static-shared/-/raw/master/nand_xor_schematic.png
[example-render]: https://gitlab.com/Ma_124/pages-static-shared/-/raw/master/nand_xor.png
[example-schematic]: examples/XOR-NAND.png
[NYAND YT]: https://youtu.be/kdCJunw_Jgg
[NYAN YT]: https://youtu.be/QH2-TGUlwu4
